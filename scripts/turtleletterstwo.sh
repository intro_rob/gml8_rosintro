#!/usr/bin/bash
rosservice call /reset
rosservice call /turtle1/set_pen 255 0 0 2 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 1.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, 2.7]'
rosservice call /spawn 6.5 5 0 "e"
rosservice call /e/set_pen 0 255 0 2 0
rostopic pub -1 /e/cmd_vel geometry_msgs/Twist -- '[1, 0, 0]' '[0, 0, .5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, 1]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, 2.7]'
rostopic pub -1 /e/cmd_vel geometry_msgs/Twist -- '[1, 0, 0]' '[0, 0, 3]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 1.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1, 0, 0]' '[0, 0, 0]'
rostopic pub -1 /e/cmd_vel geometry_msgs/Twist -- '[3, 0, 0]' '[0, 0, 3]'

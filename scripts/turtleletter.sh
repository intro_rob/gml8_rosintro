#!/usr/bin/bash
rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 1.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, 2.7]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, 1]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, 2.7]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 1.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1, 0, 0]' '[0, 0, 0]'
